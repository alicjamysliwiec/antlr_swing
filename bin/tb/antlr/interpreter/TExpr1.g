tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}


prog    :  ( e=expr
          | ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
          | ^(VAR id=ID) {globalSymbols.newSymbol($id.text);} 
          | ^(PODST id=ID e=expr) {globalSymbols.setSymbol($id.text, $e.out);}
          | ^(VAR id=ID PODST e=expr) {globalSymbols.newSymbol($id.text); globalSymbols.setSymbol($id.text, $e.out);}
          )* 
          ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = sub($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = mul($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = mod($e1.out, $e2.out);}
        | ID                       {$out = globalSymbols.getSymbol($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        ;
